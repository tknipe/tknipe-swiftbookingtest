﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using SwiftBookingTest.Model;

namespace SwiftBookingTest.DAL
{
    public class DeliveryContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }
    }
}
