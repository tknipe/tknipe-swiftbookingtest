﻿using SwiftBookingTest.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using System.Net.Http;

namespace SwiftBookingTest.Services
{
    public class Booking
    {
        public async Task<HttpResponseMessage> Book(Person p)
        {
            var msg = new 
            {
                apiKey = ConfigurationManager.AppSettings["GetSwift-MerchantKey"],
                booking = new
                {
                    pickupDetail = new
                    {
                        address = "57 luscombe st, brunswick, melbourne"
                    },
                    dropoffDetail = new
                    {
                        name = p.Name,
                        address = p.Address,
                        phone = p.Phone
                    }
                }
            };

            using (var client = new HttpClient())
            {
                var response = await client.PostAsJsonAsync("https://app.getswift.co/api/v2/deliveries", msg);
                return response;
            }
        }
    }
}
